<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class noticias extends Model
{
    protected $table = "noticias";
    protected $fillable = [
        'titulo',
        'descripcion',
        'urlfoto',
        'visitas',
    ];
}
