<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class equipos extends Model
{
    protected $table = "equipos";

    public $timestamps = false;

    protected $fillable = [
        'nombre',
        'descripcion',
        'urlfoto',
        'votos',
        'pj',
        'pg',
        'pe',
        'pp',
        'gf',
        'gc',
        'gd',
        'pts',
        'grupo'
    ];
}
