<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class calendarios extends Model
{
    protected $table = "calendarios";

    protected $fillable = [
        'fecha',
        'mes',
        'dia',
        'hora',
        'equipo_a',
        'urlogo_a',
        'equipo_b',
        'urlogo_b',
        'descripcion',
        'orden'
    ];
}
