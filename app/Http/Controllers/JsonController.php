<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\noticias;
use App\calendarios;
use App\equipos;
use Response;

class JsonController extends Controller
{
    
    public function index()
    {
        $noticias=noticias::all();
        $calendarios=calendarios::all();
        $equipos=equipos::all();
        return Response::json(
            array(
                'success'           => true,
                'message'           => "exito",
                'listaNoticias'     => $noticias,
                'listaEquipos'      => $equipos,
                'listaCalendario'   => $calendarios,
            ),200);
    
    }

    public function noticias(){
        $noticias=noticias::all();
        return Response::json(
            array(
                'success'           => true,
                'message'           => "noticias",
                'listaNoticias'     => $noticias,
            ),200);
    }
    
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
}
