<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fecha');
            $table->string('mes');
            $table->string('dia');
            $table->string('hora');
            $table->string('equipo_a');
            $table->string('urlogo_a');
            $table->string('equipo_b');
            $table->string('urlogo_b');
            $table->string('descripcion');
            $table->integer('orden');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendarios');
    }
}
