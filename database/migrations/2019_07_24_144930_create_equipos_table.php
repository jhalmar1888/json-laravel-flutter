<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->unique();   
            $table->text('descripcion')->nullable();   
            $table->string('urlfoto')->default("foto.jpg");   
            $table->integer('votos')->default(1);   
            $table->integer('pj')->nullable();   
            $table->integer('pg')->nullable();
            $table->integer('pe')->nullable();   
            $table->integer('pp')->nullable();  
            $table->integer('gf')->nullable();  
            $table->integer('gc')->nullable();  
            $table->integer('pts')->nullable();   
            $table->integer('gd')->nullable();   
            $table->string('grupo')->nullable();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
